#include <RCSwitch.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiManager.h>
#include <ArduinoOTA.h>

RCSwitch rcTransmitter = RCSwitch();
WiFiClient espClient;
PubSubClient client(espClient);

#define mqtt_server "192.168.0.58"
#define mqtt_user "homeassistant"
#define mqtt_password "2032"
#define mqtt_client "switch_controller_discovery"
#define switch_topic "control/switch/433"
int packet_repeats = 3;

void setup() {
Serial.begin(115200);
  rcTransmitter.enableTransmit(D3);
   
  setup_wifi();
  client.setServer(mqtt_server, 1883); 
  client.setCallback(process_message);
  client.subscribe(switch_topic);
  Serial.println("Subscribed to topic");
  setup_OTA();
}

void setup_wifi() {
  WiFiManager wifiManager;
  wifiManager.autoConnect(mqtt_client);
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection... ");
    if (client.connect(mqtt_client, mqtt_user, mqtt_password)) {
      Serial.println("Connected");
      client.subscribe(switch_topic);
      Serial.println("Subscribed to topic");
    } else {
      Serial.print("Failed to connect, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void process_message(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String payloadStr;
  for (int i=0;i<length;i++) {
    payloadStr = payloadStr + (char)payload[i];
  }
  Serial.print(payloadStr);
  Serial.println();
  actuateSwitch(payloadStr);
}

void actuateSwitch(String code) {
  for (int count = 0; count < packet_repeats; count++) {
    rcTransmitter.send(code.toInt(), 24);
  }
}

void loop() {
  if (!client.connected()) {
    Serial.print("Not connected to MQTT. State: ");
    Serial.println(client.state());
    reconnect();
  }
  client.loop();
}





